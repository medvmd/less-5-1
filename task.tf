variable "node_count" {
  default = "3"
}

variable "ssh_key_file_path" {
  default = "/home/user/.ssh/gcp_rsa.pub"
}

variable "ssh_private_key_file_path" {
  default = "/home/user/.ssh/gcp_rsa"
}

resource "google_compute_instance" "vm_instance" {
  count = var.node_count

  name         = "node${count.index + 1}"
  machine_type = "e2-micro"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      size  = 20
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }

  metadata = {
    ssh-keys = "user:${file(var.ssh_key_file_path)}"
  }

  provisioner "local-exec" {
    command = "echo ${self.name} ${self.network_interface.0.access_config.0.nat_ip} >> host.list"
  }

  provisioner "remote-exec" {
    connection {
      type = "ssh"
      user = "user"
      host = self.network_interface.0.access_config.0.nat_ip
      private_key = file(var.ssh_private_key_file_path)
    }
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y nginx",
      "sudo bash -c 'echo Juneway ${self.network_interface.0.access_config.0.nat_ip} ${self.name} > /var/www/html/index.html'"

    ]
  }

}
